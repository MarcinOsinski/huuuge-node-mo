
npm init -y

# Update node
curl https://www.npmjs.com/install.sh | sh

# NVM
nvm ls-remote
nvm install <version> 
nvm use <version>

# Typescript
npm i -g typescript
tsc --init

npm i @types/node

# Watch
npm i nodemon
tsc --watch ./src
nodemon -w ./dist --respawn ./dist/index.js

npm i ts-node ts-node-dev

# Modules
npm i express @types/express